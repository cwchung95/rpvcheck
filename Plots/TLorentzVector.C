void TLorentzVector()
{
//=========Macro generated from canvas: c2/c2
//=========  (Thu Jan 16 15:01:33 2020) by ROOT version 6.12/07
   TCanvas *c2 = new TCanvas("c2", "c2",0,23,1600,800);
   c2->SetHighLightColor(2);
   c2->Range(0,0,1,1);
   c2->SetFillColor(0);
   c2->SetBorderMode(0);
   c2->SetBorderSize(2);
   c2->SetFrameBorderMode(0);
  
// ------------>Primitives in pad: c2_1
   TPad *c2_1 = new TPad("c2_1", "c2_1",0.01,0.01,0.49,0.99);
   c2_1->Draw();
   c2_1->cd();
   c2_1->Range(-25,-47.54296,225,353.378);
   c2_1->SetFillColor(0);
   c2_1->SetBorderMode(0);
   c2_1->SetBorderSize(2);
   c2_1->SetFrameBorderMode(0);
   c2_1->SetFrameBorderMode(0);
   
   THStack *mll = new THStack();
   mll->SetName("mll");
   mll->SetTitle("");
   mll->SetMaximum(298.3675);
   
   TH1F *mll_stack_1 = new TH1F("mll_stack_1","",20,0,200);
   mll_stack_1->SetMinimum(-7.450859);
   mll_stack_1->SetMaximum(313.2859);
   mll_stack_1->SetDirectory(0);
   mll_stack_1->SetStats(0);

   Int_t ci;      // for color index setting
   TColor *color; // for color definition with alpha
   ci = TColor::GetColor("#000099");
   mll_stack_1->SetLineColor(ci);
   mll_stack_1->GetXaxis()->SetLabelFont(42);
   mll_stack_1->GetXaxis()->SetLabelSize(0.035);
   mll_stack_1->GetXaxis()->SetTitleSize(0.035);
   mll_stack_1->GetXaxis()->SetTitleFont(42);
   mll_stack_1->GetYaxis()->SetLabelFont(42);
   mll_stack_1->GetYaxis()->SetLabelSize(0.035);
   mll_stack_1->GetYaxis()->SetTitleSize(0.035);
   mll_stack_1->GetYaxis()->SetTitleOffset(0);
   mll_stack_1->GetYaxis()->SetTitleFont(42);
   mll_stack_1->GetZaxis()->SetLabelFont(42);
   mll_stack_1->GetZaxis()->SetLabelSize(0.035);
   mll_stack_1->GetZaxis()->SetTitleSize(0.035);
   mll_stack_1->GetZaxis()->SetTitleFont(42);
   mll->SetHistogram(mll_stack_1);
   
   
   TH1D *hmll2_stack_1 = new TH1D("hmll2_stack_1","hmll2",20,0,200);
   hmll2_stack_1->SetBinContent(1,77.01125);
   hmll2_stack_1->SetBinContent(21,0.5373872);
   hmll2_stack_1->SetBinError(1,20.54412);
   hmll2_stack_1->SetBinError(21,0.5373872);
   hmll2_stack_1->SetEntries(51);
   hmll2_stack_1->SetDirectory(0);

   ci = TColor::GetColor("#ffff66");
   hmll2_stack_1->SetFillColor(ci);

   ci = TColor::GetColor("#000099");
   hmll2_stack_1->SetLineColor(ci);
   hmll2_stack_1->GetXaxis()->SetLabelFont(42);
   hmll2_stack_1->GetXaxis()->SetLabelSize(0.035);
   hmll2_stack_1->GetXaxis()->SetTitleSize(0.035);
   hmll2_stack_1->GetXaxis()->SetTitleFont(42);
   hmll2_stack_1->GetYaxis()->SetLabelFont(42);
   hmll2_stack_1->GetYaxis()->SetLabelSize(0.035);
   hmll2_stack_1->GetYaxis()->SetTitleSize(0.035);
   hmll2_stack_1->GetYaxis()->SetTitleOffset(0);
   hmll2_stack_1->GetYaxis()->SetTitleFont(42);
   hmll2_stack_1->GetZaxis()->SetLabelFont(42);
   hmll2_stack_1->GetZaxis()->SetLabelSize(0.035);
   hmll2_stack_1->GetZaxis()->SetTitleSize(0.035);
   hmll2_stack_1->GetZaxis()->SetTitleFont(42);
   mll->Add(hmll2_stack_1,"");
   
   TH1D *hmll2_stack_2 = new TH1D("hmll2_stack_2","hmll2",20,0,200);
   hmll2_stack_2->SetBinContent(1,1.046252);
   hmll2_stack_2->SetBinContent(2,1.74696);
   hmll2_stack_2->SetBinContent(3,6.068464);
   hmll2_stack_2->SetBinContent(4,5.415938);
   hmll2_stack_2->SetBinContent(5,7.977088);
   hmll2_stack_2->SetBinContent(6,7.420603);
   hmll2_stack_2->SetBinContent(7,6.971528);
   hmll2_stack_2->SetBinContent(8,6.599142);
   hmll2_stack_2->SetBinContent(9,5.878661);
   hmll2_stack_2->SetBinContent(10,8.121621);
   hmll2_stack_2->SetBinContent(11,10.31978);
   hmll2_stack_2->SetBinContent(12,10.99151);
   hmll2_stack_2->SetBinContent(13,8.908127);
   hmll2_stack_2->SetBinContent(14,4.704712);
   hmll2_stack_2->SetBinContent(15,5.382187);
   hmll2_stack_2->SetBinContent(16,4.199432);
   hmll2_stack_2->SetBinContent(17,3.543853);
   hmll2_stack_2->SetBinContent(18,4.02259);
   hmll2_stack_2->SetBinContent(19,4.835399);
   hmll2_stack_2->SetBinContent(20,5.984994);
   hmll2_stack_2->SetBinContent(21,34.58644);
   hmll2_stack_2->SetBinError(1,0.7439447);
   hmll2_stack_2->SetBinError(2,0.8810705);
   hmll2_stack_2->SetBinError(3,1.645806);
   hmll2_stack_2->SetBinError(4,1.553643);
   hmll2_stack_2->SetBinError(5,1.853136);
   hmll2_stack_2->SetBinError(6,1.75061);
   hmll2_stack_2->SetBinError(7,1.722584);
   hmll2_stack_2->SetBinError(8,1.721526);
   hmll2_stack_2->SetBinError(9,1.554339);
   hmll2_stack_2->SetBinError(10,1.878922);
   hmll2_stack_2->SetBinError(11,2.131731);
   hmll2_stack_2->SetBinError(12,2.119311);
   hmll2_stack_2->SetBinError(13,2.059966);
   hmll2_stack_2->SetBinError(14,1.355638);
   hmll2_stack_2->SetBinError(15,1.566439);
   hmll2_stack_2->SetBinError(16,1.36342);
   hmll2_stack_2->SetBinError(17,1.254332);
   hmll2_stack_2->SetBinError(18,1.345476);
   hmll2_stack_2->SetBinError(19,1.404266);
   hmll2_stack_2->SetBinError(20,1.617791);
   hmll2_stack_2->SetBinError(21,3.769785);
   hmll2_stack_2->SetEntries(392);
   hmll2_stack_2->SetDirectory(0);

   ci = TColor::GetColor("#0099ff");
   hmll2_stack_2->SetFillColor(ci);

   ci = TColor::GetColor("#000099");
   hmll2_stack_2->SetLineColor(ci);
   hmll2_stack_2->GetXaxis()->SetLabelFont(42);
   hmll2_stack_2->GetXaxis()->SetLabelSize(0.035);
   hmll2_stack_2->GetXaxis()->SetTitleSize(0.035);
   hmll2_stack_2->GetXaxis()->SetTitleFont(42);
   hmll2_stack_2->GetYaxis()->SetLabelFont(42);
   hmll2_stack_2->GetYaxis()->SetLabelSize(0.035);
   hmll2_stack_2->GetYaxis()->SetTitleSize(0.035);
   hmll2_stack_2->GetYaxis()->SetTitleOffset(0);
   hmll2_stack_2->GetYaxis()->SetTitleFont(42);
   hmll2_stack_2->GetZaxis()->SetLabelFont(42);
   hmll2_stack_2->GetZaxis()->SetLabelSize(0.035);
   hmll2_stack_2->GetZaxis()->SetTitleSize(0.035);
   hmll2_stack_2->GetZaxis()->SetTitleFont(42);
   mll->Add(hmll2_stack_2,"");
   
   TH1D *hmll2_stack_3 = new TH1D("hmll2_stack_3","hmll2",20,0,200);
   hmll2_stack_3->SetDirectory(0);

   ci = TColor::GetColor("#009900");
   hmll2_stack_3->SetFillColor(ci);

   ci = TColor::GetColor("#000099");
   hmll2_stack_3->SetLineColor(ci);
   hmll2_stack_3->GetXaxis()->SetLabelFont(42);
   hmll2_stack_3->GetXaxis()->SetLabelSize(0.035);
   hmll2_stack_3->GetXaxis()->SetTitleSize(0.035);
   hmll2_stack_3->GetXaxis()->SetTitleFont(42);
   hmll2_stack_3->GetYaxis()->SetLabelFont(42);
   hmll2_stack_3->GetYaxis()->SetLabelSize(0.035);
   hmll2_stack_3->GetYaxis()->SetTitleSize(0.035);
   hmll2_stack_3->GetYaxis()->SetTitleOffset(0);
   hmll2_stack_3->GetYaxis()->SetTitleFont(42);
   hmll2_stack_3->GetZaxis()->SetLabelFont(42);
   hmll2_stack_3->GetZaxis()->SetLabelSize(0.035);
   hmll2_stack_3->GetZaxis()->SetTitleSize(0.035);
   hmll2_stack_3->GetZaxis()->SetTitleFont(42);
   mll->Add(hmll2_stack_3,"");
   
   TH1D *hmll2_stack_4 = new TH1D("hmll2_stack_4","hmll2",20,0,200);
   hmll2_stack_4->SetBinContent(2,3.348782);
   hmll2_stack_4->SetBinContent(3,19.0757);
   hmll2_stack_4->SetBinContent(5,-15.96077);
   hmll2_stack_4->SetBinContent(7,12.2513);
   hmll2_stack_4->SetBinContent(8,25.24376);
   hmll2_stack_4->SetBinContent(9,150.5651);
   hmll2_stack_4->SetBinContent(10,216.4807);
   hmll2_stack_4->SetBinContent(11,23.56371);
   hmll2_stack_4->SetBinContent(13,0.2255437);
   hmll2_stack_4->SetBinContent(19,4.291331);
   hmll2_stack_4->SetBinContent(20,4.409832);
   hmll2_stack_4->SetBinContent(21,5.429092);
   hmll2_stack_4->SetBinError(2,23.82317);
   hmll2_stack_4->SetBinError(3,19.0757);
   hmll2_stack_4->SetBinError(5,15.96077);
   hmll2_stack_4->SetBinError(7,7.154113);
   hmll2_stack_4->SetBinError(8,11.386);
   hmll2_stack_4->SetBinError(9,28.33634);
   hmll2_stack_4->SetBinError(10,33.07296);
   hmll2_stack_4->SetBinError(11,11.82051);
   hmll2_stack_4->SetBinError(13,0.2255437);
   hmll2_stack_4->SetBinError(19,4.291331);
   hmll2_stack_4->SetBinError(20,4.409832);
   hmll2_stack_4->SetBinError(21,4.746247);
   hmll2_stack_4->SetEntries(97);
   hmll2_stack_4->SetDirectory(0);

   ci = TColor::GetColor("#ff6666");
   hmll2_stack_4->SetFillColor(ci);

   ci = TColor::GetColor("#000099");
   hmll2_stack_4->SetLineColor(ci);
   hmll2_stack_4->GetXaxis()->SetLabelFont(42);
   hmll2_stack_4->GetXaxis()->SetLabelSize(0.035);
   hmll2_stack_4->GetXaxis()->SetTitleSize(0.035);
   hmll2_stack_4->GetXaxis()->SetTitleFont(42);
   hmll2_stack_4->GetYaxis()->SetLabelFont(42);
   hmll2_stack_4->GetYaxis()->SetLabelSize(0.035);
   hmll2_stack_4->GetYaxis()->SetTitleSize(0.035);
   hmll2_stack_4->GetYaxis()->SetTitleOffset(0);
   hmll2_stack_4->GetYaxis()->SetTitleFont(42);
   hmll2_stack_4->GetZaxis()->SetLabelFont(42);
   hmll2_stack_4->GetZaxis()->SetLabelSize(0.035);
   hmll2_stack_4->GetZaxis()->SetTitleSize(0.035);
   hmll2_stack_4->GetZaxis()->SetTitleFont(42);
   mll->Add(hmll2_stack_4,"");
   
   TH1D *hmll2_stack_5 = new TH1D("hmll2_stack_5","hmll2",20,0,200);
   hmll2_stack_5->SetBinContent(1,0.3335131);
   hmll2_stack_5->SetBinContent(2,0.182571);
   hmll2_stack_5->SetBinContent(3,0.7248334);
   hmll2_stack_5->SetBinContent(4,0.1294593);
   hmll2_stack_5->SetBinContent(5,0.5328279);
   hmll2_stack_5->SetBinContent(6,0.497175);
   hmll2_stack_5->SetBinContent(7,0.5997806);
   hmll2_stack_5->SetBinContent(8,1.015998);
   hmll2_stack_5->SetBinContent(9,4.320754);
   hmll2_stack_5->SetBinContent(10,4.911221);
   hmll2_stack_5->SetBinContent(11,0.5944213);
   hmll2_stack_5->SetBinContent(12,0.4892727);
   hmll2_stack_5->SetBinContent(13,0.3102731);
   hmll2_stack_5->SetBinContent(14,0.3951885);
   hmll2_stack_5->SetBinContent(15,0.4769721);
   hmll2_stack_5->SetBinContent(16,1.45943);
   hmll2_stack_5->SetBinContent(17,0.3643044);
   hmll2_stack_5->SetBinContent(18,2.323265);
   hmll2_stack_5->SetBinContent(19,1.048817);
   hmll2_stack_5->SetBinContent(20,0.166532);
   hmll2_stack_5->SetBinContent(21,8.832494);
   hmll2_stack_5->SetBinError(1,0.2421814);
   hmll2_stack_5->SetBinError(2,0.1356673);
   hmll2_stack_5->SetBinError(3,0.3736909);
   hmll2_stack_5->SetBinError(4,0.08359195);
   hmll2_stack_5->SetBinError(5,0.2376898);
   hmll2_stack_5->SetBinError(6,0.2036698);
   hmll2_stack_5->SetBinError(7,0.3395917);
   hmll2_stack_5->SetBinError(8,0.2919107);
   hmll2_stack_5->SetBinError(9,1.611924);
   hmll2_stack_5->SetBinError(10,1.884043);
   hmll2_stack_5->SetBinError(11,0.229509);
   hmll2_stack_5->SetBinError(12,0.1363092);
   hmll2_stack_5->SetBinError(13,0.1116285);
   hmll2_stack_5->SetBinError(14,0.2452025);
   hmll2_stack_5->SetBinError(15,0.2317656);
   hmll2_stack_5->SetBinError(16,0.5384564);
   hmll2_stack_5->SetBinError(17,0.2047006);
   hmll2_stack_5->SetBinError(18,1.751828);
   hmll2_stack_5->SetBinError(19,0.4584114);
   hmll2_stack_5->SetBinError(20,0.09308406);
   hmll2_stack_5->SetBinError(21,1.300967);
   hmll2_stack_5->SetEntries(17952);

   ci = TColor::GetColor("#999999");
   hmll2_stack_5->SetFillColor(ci);

   ci = TColor::GetColor("#000099");
   hmll2_stack_5->SetLineColor(ci);
   hmll2_stack_5->GetXaxis()->SetLabelFont(42);
   hmll2_stack_5->GetXaxis()->SetLabelSize(0.035);
   hmll2_stack_5->GetXaxis()->SetTitleSize(0.035);
   hmll2_stack_5->GetXaxis()->SetTitleFont(42);
   hmll2_stack_5->GetYaxis()->SetLabelFont(42);
   hmll2_stack_5->GetYaxis()->SetLabelSize(0.035);
   hmll2_stack_5->GetYaxis()->SetTitleSize(0.035);
   hmll2_stack_5->GetYaxis()->SetTitleOffset(0);
   hmll2_stack_5->GetYaxis()->SetTitleFont(42);
   hmll2_stack_5->GetZaxis()->SetLabelFont(42);
   hmll2_stack_5->GetZaxis()->SetLabelSize(0.035);
   hmll2_stack_5->GetZaxis()->SetTitleSize(0.035);
   hmll2_stack_5->GetZaxis()->SetTitleFont(42);
   mll->Add(hmll2_stack_5,"");
   mll->Draw("hist");
   
   TH1D *hmll2__1 = new TH1D("hmll2__1","hmll2",20,0,200);
   hmll2__1->SetBinContent(1,40);
   hmll2__1->SetBinContent(2,12);
   hmll2__1->SetBinContent(3,7);
   hmll2__1->SetBinContent(4,6);
   hmll2__1->SetBinContent(5,10);
   hmll2__1->SetBinContent(6,13);
   hmll2__1->SetBinContent(7,9);
   hmll2__1->SetBinContent(8,22);
   hmll2__1->SetBinContent(9,155);
   hmll2__1->SetBinContent(10,205);
   hmll2__1->SetBinContent(11,15);
   hmll2__1->SetBinContent(12,4);
   hmll2__1->SetBinContent(13,10);
   hmll2__1->SetBinContent(14,8);
   hmll2__1->SetBinContent(15,5);
   hmll2__1->SetBinContent(16,10);
   hmll2__1->SetBinContent(17,5);
   hmll2__1->SetBinContent(19,2);
   hmll2__1->SetBinContent(20,1);
   hmll2__1->SetBinContent(21,33);
   hmll2__1->SetEntries(572);
   hmll2__1->SetDirectory(0);

   ci = TColor::GetColor("#000099");
   hmll2__1->SetLineColor(ci);
   hmll2__1->GetXaxis()->SetLabelFont(42);
   hmll2__1->GetXaxis()->SetLabelSize(0.035);
   hmll2__1->GetXaxis()->SetTitleSize(0.035);
   hmll2__1->GetXaxis()->SetTitleFont(42);
   hmll2__1->GetYaxis()->SetLabelFont(42);
   hmll2__1->GetYaxis()->SetLabelSize(0.035);
   hmll2__1->GetYaxis()->SetTitleSize(0.035);
   hmll2__1->GetYaxis()->SetTitleOffset(0);
   hmll2__1->GetYaxis()->SetTitleFont(42);
   hmll2__1->GetZaxis()->SetLabelFont(42);
   hmll2__1->GetZaxis()->SetLabelSize(0.035);
   hmll2__1->GetZaxis()->SetTitleSize(0.035);
   hmll2__1->GetZaxis()->SetTitleFont(42);
   hmll2__1->Draw("same e");
   c2_1->Modified();
   c2->cd();
  
// ------------>Primitives in pad: c2_2
   TPad *c2_2 = new TPad("c2_2", "c2_2",0.51,0.01,0.99,0.99);
   c2_2->Draw();
   c2_2->cd();
   c2_2->Range(3.25,-39.52117,10.75,355.6905);
   c2_2->SetFillColor(0);
   c2_2->SetBorderMode(0);
   c2_2->SetBorderSize(2);
   c2_2->SetFrameBorderMode(0);
   c2_2->SetFrameBorderMode(0);
   
   njets = new THStack();
   njets->SetName("njets");
   njets->SetTitle("");
   njets->SetMaximum(301.1137);
   
   TH1F *njets_stack_2 = new TH1F("njets_stack_2","",3,4,10);
   njets_stack_2->SetMinimum(0);
   njets_stack_2->SetMaximum(316.1694);
   njets_stack_2->SetDirectory(0);
   njets_stack_2->SetStats(0);

   ci = TColor::GetColor("#000099");
   njets_stack_2->SetLineColor(ci);
   njets_stack_2->GetXaxis()->SetLabelFont(42);
   njets_stack_2->GetXaxis()->SetLabelSize(0.035);
   njets_stack_2->GetXaxis()->SetTitleSize(0.035);
   njets_stack_2->GetXaxis()->SetTitleFont(42);
   njets_stack_2->GetYaxis()->SetLabelFont(42);
   njets_stack_2->GetYaxis()->SetLabelSize(0.035);
   njets_stack_2->GetYaxis()->SetTitleSize(0.035);
   njets_stack_2->GetYaxis()->SetTitleOffset(0);
   njets_stack_2->GetYaxis()->SetTitleFont(42);
   njets_stack_2->GetZaxis()->SetLabelFont(42);
   njets_stack_2->GetZaxis()->SetLabelSize(0.035);
   njets_stack_2->GetZaxis()->SetTitleSize(0.035);
   njets_stack_2->GetZaxis()->SetTitleFont(42);
   njets->SetHistogram(njets_stack_2);
   
   
   TH1D *hnjets_stack_1 = new TH1D("hnjets_stack_1","hnjets",3,4,10);
   hnjets_stack_1->SetDirectory(0);

   ci = TColor::GetColor("#ffff66");
   hnjets_stack_1->SetFillColor(ci);

   ci = TColor::GetColor("#000099");
   hnjets_stack_1->SetLineColor(ci);
   hnjets_stack_1->GetXaxis()->SetLabelFont(42);
   hnjets_stack_1->GetXaxis()->SetLabelSize(0.035);
   hnjets_stack_1->GetXaxis()->SetTitleSize(0.035);
   hnjets_stack_1->GetXaxis()->SetTitleFont(42);
   hnjets_stack_1->GetYaxis()->SetLabelFont(42);
   hnjets_stack_1->GetYaxis()->SetLabelSize(0.035);
   hnjets_stack_1->GetYaxis()->SetTitleSize(0.035);
   hnjets_stack_1->GetYaxis()->SetTitleOffset(0);
   hnjets_stack_1->GetYaxis()->SetTitleFont(42);
   hnjets_stack_1->GetZaxis()->SetLabelFont(42);
   hnjets_stack_1->GetZaxis()->SetLabelSize(0.035);
   hnjets_stack_1->GetZaxis()->SetTitleSize(0.035);
   hnjets_stack_1->GetZaxis()->SetTitleFont(42);
   njets->Add(hnjets_stack_1,"");
   
   TH1D *hnjets_stack_2 = new TH1D("hnjets_stack_2","hnjets",3,4,10);
   hnjets_stack_2->SetBinContent(1,5.824367);
   hnjets_stack_2->SetBinContent(2,6.220631);
   hnjets_stack_2->SetBinContent(3,1.955284);
   hnjets_stack_2->SetBinError(1,1.563233);
   hnjets_stack_2->SetBinError(2,1.667143);
   hnjets_stack_2->SetBinError(3,0.8504453);
   hnjets_stack_2->SetEntries(36);
   hnjets_stack_2->SetDirectory(0);

   ci = TColor::GetColor("#0099ff");
   hnjets_stack_2->SetFillColor(ci);

   ci = TColor::GetColor("#000099");
   hnjets_stack_2->SetLineColor(ci);
   hnjets_stack_2->GetXaxis()->SetLabelFont(42);
   hnjets_stack_2->GetXaxis()->SetLabelSize(0.035);
   hnjets_stack_2->GetXaxis()->SetTitleSize(0.035);
   hnjets_stack_2->GetXaxis()->SetTitleFont(42);
   hnjets_stack_2->GetYaxis()->SetLabelFont(42);
   hnjets_stack_2->GetYaxis()->SetLabelSize(0.035);
   hnjets_stack_2->GetYaxis()->SetTitleSize(0.035);
   hnjets_stack_2->GetYaxis()->SetTitleOffset(0);
   hnjets_stack_2->GetYaxis()->SetTitleFont(42);
   hnjets_stack_2->GetZaxis()->SetLabelFont(42);
   hnjets_stack_2->GetZaxis()->SetLabelSize(0.035);
   hnjets_stack_2->GetZaxis()->SetTitleSize(0.035);
   hnjets_stack_2->GetZaxis()->SetTitleFont(42);
   njets->Add(hnjets_stack_2,"");
   
   TH1D *hnjets_stack_3 = new TH1D("hnjets_stack_3","hnjets",3,4,10);
   hnjets_stack_3->SetDirectory(0);

   ci = TColor::GetColor("#009900");
   hnjets_stack_3->SetFillColor(ci);

   ci = TColor::GetColor("#000099");
   hnjets_stack_3->SetLineColor(ci);
   hnjets_stack_3->GetXaxis()->SetLabelFont(42);
   hnjets_stack_3->GetXaxis()->SetLabelSize(0.035);
   hnjets_stack_3->GetXaxis()->SetTitleSize(0.035);
   hnjets_stack_3->GetXaxis()->SetTitleFont(42);
   hnjets_stack_3->GetYaxis()->SetLabelFont(42);
   hnjets_stack_3->GetYaxis()->SetLabelSize(0.035);
   hnjets_stack_3->GetYaxis()->SetTitleSize(0.035);
   hnjets_stack_3->GetYaxis()->SetTitleOffset(0);
   hnjets_stack_3->GetYaxis()->SetTitleFont(42);
   hnjets_stack_3->GetZaxis()->SetLabelFont(42);
   hnjets_stack_3->GetZaxis()->SetLabelSize(0.035);
   hnjets_stack_3->GetZaxis()->SetTitleSize(0.035);
   hnjets_stack_3->GetZaxis()->SetTitleFont(42);
   njets->Add(hnjets_stack_3,"");
   
   TH1D *hnjets_stack_4 = new TH1D("hnjets_stack_4","hnjets",3,4,10);
   hnjets_stack_4->SetBinContent(1,222.9089);
   hnjets_stack_4->SetBinContent(2,97.17084);
   hnjets_stack_4->SetBinContent(3,46.96602);
   hnjets_stack_4->SetBinError(1,33.63275);
   hnjets_stack_4->SetBinError(2,21.99876);
   hnjets_stack_4->SetBinError(3,16.78277);
   hnjets_stack_4->SetEntries(76);
   hnjets_stack_4->SetDirectory(0);

   ci = TColor::GetColor("#ff6666");
   hnjets_stack_4->SetFillColor(ci);

   ci = TColor::GetColor("#000099");
   hnjets_stack_4->SetLineColor(ci);
   hnjets_stack_4->GetXaxis()->SetLabelFont(42);
   hnjets_stack_4->GetXaxis()->SetLabelSize(0.035);
   hnjets_stack_4->GetXaxis()->SetTitleSize(0.035);
   hnjets_stack_4->GetXaxis()->SetTitleFont(42);
   hnjets_stack_4->GetYaxis()->SetLabelFont(42);
   hnjets_stack_4->GetYaxis()->SetLabelSize(0.035);
   hnjets_stack_4->GetYaxis()->SetTitleSize(0.035);
   hnjets_stack_4->GetYaxis()->SetTitleOffset(0);
   hnjets_stack_4->GetYaxis()->SetTitleFont(42);
   hnjets_stack_4->GetZaxis()->SetLabelFont(42);
   hnjets_stack_4->GetZaxis()->SetLabelSize(0.035);
   hnjets_stack_4->GetZaxis()->SetTitleSize(0.035);
   hnjets_stack_4->GetZaxis()->SetTitleFont(42);
   njets->Add(hnjets_stack_4,"");
   
   TH1D *hnjets_stack_5 = new TH1D("hnjets_stack_5","hnjets",3,4,10);
   hnjets_stack_5->SetBinContent(0,0.01020962);
   hnjets_stack_5->SetBinContent(1,2.892616);
   hnjets_stack_5->SetBinContent(2,2.63981);
   hnjets_stack_5->SetBinContent(3,3.689339);
   hnjets_stack_5->SetBinError(0,0.005946412);
   hnjets_stack_5->SetBinError(1,1.859909);
   hnjets_stack_5->SetBinError(2,0.3537467);
   hnjets_stack_5->SetBinError(3,1.601089);
   hnjets_stack_5->SetEntries(7206);

   ci = TColor::GetColor("#999999");
   hnjets_stack_5->SetFillColor(ci);

   ci = TColor::GetColor("#000099");
   hnjets_stack_5->SetLineColor(ci);
   hnjets_stack_5->GetXaxis()->SetLabelFont(42);
   hnjets_stack_5->GetXaxis()->SetLabelSize(0.035);
   hnjets_stack_5->GetXaxis()->SetTitleSize(0.035);
   hnjets_stack_5->GetXaxis()->SetTitleFont(42);
   hnjets_stack_5->GetYaxis()->SetLabelFont(42);
   hnjets_stack_5->GetYaxis()->SetLabelSize(0.035);
   hnjets_stack_5->GetYaxis()->SetTitleSize(0.035);
   hnjets_stack_5->GetYaxis()->SetTitleOffset(0);
   hnjets_stack_5->GetYaxis()->SetTitleFont(42);
   hnjets_stack_5->GetZaxis()->SetLabelFont(42);
   hnjets_stack_5->GetZaxis()->SetLabelSize(0.035);
   hnjets_stack_5->GetZaxis()->SetTitleSize(0.035);
   hnjets_stack_5->GetZaxis()->SetTitleFont(42);
   njets->Add(hnjets_stack_5,"");
   njets->Draw("hist");
   
   TH1D *hnjets__2 = new TH1D("hnjets__2","hnjets",3,4,10);
   hnjets__2->SetBinContent(0,1);
   hnjets__2->SetBinContent(1,182);
   hnjets__2->SetBinContent(2,138);
   hnjets__2->SetBinContent(3,37);
   hnjets__2->SetEntries(358);
   hnjets__2->SetDirectory(0);

   ci = TColor::GetColor("#000099");
   hnjets__2->SetLineColor(ci);
   hnjets__2->GetXaxis()->SetLabelFont(42);
   hnjets__2->GetXaxis()->SetLabelSize(0.035);
   hnjets__2->GetXaxis()->SetTitleSize(0.035);
   hnjets__2->GetXaxis()->SetTitleFont(42);
   hnjets__2->GetYaxis()->SetLabelFont(42);
   hnjets__2->GetYaxis()->SetLabelSize(0.035);
   hnjets__2->GetYaxis()->SetTitleSize(0.035);
   hnjets__2->GetYaxis()->SetTitleOffset(0);
   hnjets__2->GetYaxis()->SetTitleFont(42);
   hnjets__2->GetZaxis()->SetLabelFont(42);
   hnjets__2->GetZaxis()->SetLabelSize(0.035);
   hnjets__2->GetZaxis()->SetTitleSize(0.035);
   hnjets__2->GetZaxis()->SetTitleFont(42);
   hnjets__2->Draw("same e");
   c2_2->Modified();
   c2->cd();
   c2->Modified();
   c2->cd();
   c2->SetSelected(c2);
}
