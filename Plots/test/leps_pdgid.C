void leps_pdgid()
{
//=========Macro generated from canvas: c1/c1
//=========  (Thu Dec 26 14:34:49 2019) by ROOT version 6.12/07
   TCanvas *c1 = new TCanvas("c1", "c1",0,23,2400,800);
   c1->SetHighLightColor(2);
   c1->Range(0,0,1,1);
   c1->SetFillColor(0);
   c1->SetBorderMode(0);
   c1->SetBorderSize(2);
   c1->SetFrameBorderMode(0);
  
// ------------>Primitives in pad: c1_1
   TPad *c1_1 = new TPad("c1_1", "c1_1",0.01,0.01,0.3233333,0.99);
   c1_1->Draw();
   c1_1->cd();
   c1_1->Range(-18.75,-8.925001,18.75,80.325);
   c1_1->SetFillColor(0);
   c1_1->SetBorderMode(0);
   c1_1->SetBorderSize(2);
   c1_1->SetFrameBorderMode(0);
   c1_1->SetFrameBorderMode(0);
   
   TH1D *hbkg__43 = new TH1D("hbkg__43","leps_pdgid",30,-15,15);
   hbkg__43->SetBinContent(3,68);
   hbkg__43->SetBinContent(5,49);
   hbkg__43->SetBinContent(27,67);
   hbkg__43->SetBinContent(29,68);
   hbkg__43->SetMinimum(0);
   hbkg__43->SetEntries(252);
   hbkg__43->SetStats(0);

   Int_t ci;      // for color index setting
   TColor *color; // for color definition with alpha
   ci = TColor::GetColor("#000099");
   hbkg__43->SetLineColor(ci);
   hbkg__43->GetXaxis()->SetRange(1,99);
   hbkg__43->GetXaxis()->SetLabelFont(42);
   hbkg__43->GetXaxis()->SetLabelSize(0.035);
   hbkg__43->GetXaxis()->SetTitleSize(0.035);
   hbkg__43->GetXaxis()->SetTitleFont(42);
   hbkg__43->GetYaxis()->SetLabelFont(42);
   hbkg__43->GetYaxis()->SetLabelSize(0.035);
   hbkg__43->GetYaxis()->SetTitleSize(0.035);
   hbkg__43->GetYaxis()->SetTitleOffset(0);
   hbkg__43->GetYaxis()->SetTitleFont(42);
   hbkg__43->GetZaxis()->SetLabelFont(42);
   hbkg__43->GetZaxis()->SetLabelSize(0.035);
   hbkg__43->GetZaxis()->SetTitleSize(0.035);
   hbkg__43->GetZaxis()->SetTitleFont(42);
   hbkg__43->Draw("");
   
   TPaveText *pt = new TPaveText(0.3542486,0.9348829,0.6457514,0.995,"blNDC");
   pt->SetName("title");
   pt->SetBorderSize(0);
   pt->SetFillColor(0);
   pt->SetFillStyle(0);
   pt->SetTextFont(42);
   TText *pt_LaTex = pt->AddText("leps_pdgid");
   pt->Draw();
   c1_1->Modified();
   c1->cd();
  
// ------------>Primitives in pad: c1_2
   TPad *c1_2 = new TPad("c1_2", "c1_2",0.3433333,0.01,0.6566667,0.99);
   c1_2->Draw();
   c1_2->cd();
   c1_2->Range(-18.75,-13.3875,18.75,120.4875);
   c1_2->SetFillColor(0);
   c1_2->SetBorderMode(0);
   c1_2->SetBorderSize(2);
   c1_2->SetFrameBorderMode(0);
   c1_2->SetFrameBorderMode(0);
   
   TH1D *hsig__44 = new TH1D("hsig__44","leps_pdgid",30,-15,15);
   hsig__44->SetBinContent(3,102);
   hsig__44->SetBinContent(5,70);
   hsig__44->SetBinContent(27,4);
   hsig__44->SetBinContent(29,3);
   hsig__44->SetMinimum(0);
   hsig__44->SetEntries(179);
   hsig__44->SetStats(0);

   ci = TColor::GetColor("#000099");
   hsig__44->SetLineColor(ci);
   hsig__44->GetXaxis()->SetRange(1,99);
   hsig__44->GetXaxis()->SetLabelFont(42);
   hsig__44->GetXaxis()->SetLabelSize(0.035);
   hsig__44->GetXaxis()->SetTitleSize(0.035);
   hsig__44->GetXaxis()->SetTitleFont(42);
   hsig__44->GetYaxis()->SetLabelFont(42);
   hsig__44->GetYaxis()->SetLabelSize(0.035);
   hsig__44->GetYaxis()->SetTitleSize(0.035);
   hsig__44->GetYaxis()->SetTitleOffset(0);
   hsig__44->GetYaxis()->SetTitleFont(42);
   hsig__44->GetZaxis()->SetLabelFont(42);
   hsig__44->GetZaxis()->SetLabelSize(0.035);
   hsig__44->GetZaxis()->SetTitleSize(0.035);
   hsig__44->GetZaxis()->SetTitleFont(42);
   hsig__44->Draw("");
   
   pt = new TPaveText(0.3542486,0.9348829,0.6457514,0.995,"blNDC");
   pt->SetName("title");
   pt->SetBorderSize(0);
   pt->SetFillColor(0);
   pt->SetFillStyle(0);
   pt->SetTextFont(42);
   pt_LaTex = pt->AddText("leps_pdgid");
   pt->Draw();
   c1_2->Modified();
   c1->cd();
  
// ------------>Primitives in pad: c1_3
   TPad *c1_3 = new TPad("c1_3", "c1_3",0.6766667,0.01,0.99,0.99);
   c1_3->Draw();
   c1_3->cd();
   c1_3->Range(-18.75,-1.96875,18.75,17.71875);
   c1_3->SetFillColor(0);
   c1_3->SetBorderMode(0);
   c1_3->SetBorderSize(2);
   c1_3->SetFrameBorderMode(0);
   c1_3->SetFrameBorderMode(0);
   
   TH1D *hdat__45 = new TH1D("hdat__45","leps_pdgid",30,-15,15);
   hdat__45->SetBinContent(3,13);
   hdat__45->SetBinContent(5,15);
   hdat__45->SetBinContent(27,11);
   hdat__45->SetBinContent(29,10);
   hdat__45->SetMinimum(0);
   hdat__45->SetEntries(49);
   hdat__45->SetStats(0);

   ci = TColor::GetColor("#000099");
   hdat__45->SetLineColor(ci);
   hdat__45->GetXaxis()->SetRange(1,99);
   hdat__45->GetXaxis()->SetLabelFont(42);
   hdat__45->GetXaxis()->SetLabelSize(0.035);
   hdat__45->GetXaxis()->SetTitleSize(0.035);
   hdat__45->GetXaxis()->SetTitleFont(42);
   hdat__45->GetYaxis()->SetLabelFont(42);
   hdat__45->GetYaxis()->SetLabelSize(0.035);
   hdat__45->GetYaxis()->SetTitleSize(0.035);
   hdat__45->GetYaxis()->SetTitleOffset(0);
   hdat__45->GetYaxis()->SetTitleFont(42);
   hdat__45->GetZaxis()->SetLabelFont(42);
   hdat__45->GetZaxis()->SetLabelSize(0.035);
   hdat__45->GetZaxis()->SetTitleSize(0.035);
   hdat__45->GetZaxis()->SetTitleFont(42);
   hdat__45->Draw("");
   
   pt = new TPaveText(0.3542486,0.9348829,0.6457514,0.995,"blNDC");
   pt->SetName("title");
   pt->SetBorderSize(0);
   pt->SetFillColor(0);
   pt->SetFillStyle(0);
   pt->SetTextFont(42);
   pt_LaTex = pt->AddText("leps_pdgid");
   pt->Draw();
   c1_3->Modified();
   c1->cd();
   c1->Modified();
   c1->cd();
   c1->SetSelected(c1);
}
