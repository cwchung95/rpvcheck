void mus_sigid()
{
//=========Macro generated from canvas: c1/c1
//=========  (Thu Dec 26 14:36:31 2019) by ROOT version 6.12/07
   TCanvas *c1 = new TCanvas("c1", "c1",0,23,2400,800);
   c1->SetHighLightColor(2);
   c1->Range(0,0,1,1);
   c1->SetFillColor(0);
   c1->SetBorderMode(0);
   c1->SetBorderSize(2);
   c1->SetFrameBorderMode(0);
  
// ------------>Primitives in pad: c1_1
   TPad *c1_1 = new TPad("c1_1", "c1_1",0.01,0.01,0.3233333,0.99);
   c1_1->Draw();
   c1_1->cd();
   c1_1->Range(-0.25,-107.4938,2.25,967.4438);
   c1_1->SetFillColor(0);
   c1_1->SetBorderMode(0);
   c1_1->SetBorderSize(2);
   c1_1->SetFrameBorderMode(0);
   c1_1->SetFrameBorderMode(0);
   
   TH1D *hbkg__61 = new TH1D("hbkg__61","mus_sigid",2,0,2);
   hbkg__61->SetBinContent(1,157);
   hbkg__61->SetBinContent(2,819);
   hbkg__61->SetMinimum(0);
   hbkg__61->SetEntries(976);
   hbkg__61->SetStats(0);

   Int_t ci;      // for color index setting
   TColor *color; // for color definition with alpha
   ci = TColor::GetColor("#000099");
   hbkg__61->SetLineColor(ci);
   hbkg__61->GetXaxis()->SetRange(1,99);
   hbkg__61->GetXaxis()->SetLabelFont(42);
   hbkg__61->GetXaxis()->SetLabelSize(0.035);
   hbkg__61->GetXaxis()->SetTitleSize(0.035);
   hbkg__61->GetXaxis()->SetTitleFont(42);
   hbkg__61->GetYaxis()->SetLabelFont(42);
   hbkg__61->GetYaxis()->SetLabelSize(0.035);
   hbkg__61->GetYaxis()->SetTitleSize(0.035);
   hbkg__61->GetYaxis()->SetTitleOffset(0);
   hbkg__61->GetYaxis()->SetTitleFont(42);
   hbkg__61->GetZaxis()->SetLabelFont(42);
   hbkg__61->GetZaxis()->SetLabelSize(0.035);
   hbkg__61->GetZaxis()->SetTitleSize(0.035);
   hbkg__61->GetZaxis()->SetTitleFont(42);
   hbkg__61->Draw("");
   
   TPaveText *pt = new TPaveText(0.3628994,0.9348829,0.6371006,0.995,"blNDC");
   pt->SetName("title");
   pt->SetBorderSize(0);
   pt->SetFillColor(0);
   pt->SetFillStyle(0);
   pt->SetTextFont(42);
   TText *pt_LaTex = pt->AddText("mus_sigid");
   pt->Draw();
   c1_1->Modified();
   c1->cd();
  
// ------------>Primitives in pad: c1_2
   TPad *c1_2 = new TPad("c1_2", "c1_2",0.3433333,0.01,0.6566667,0.99);
   c1_2->Draw();
   c1_2->cd();
   c1_2->Range(-0.25,-81.50626,2.25,733.5563);
   c1_2->SetFillColor(0);
   c1_2->SetBorderMode(0);
   c1_2->SetBorderSize(2);
   c1_2->SetFrameBorderMode(0);
   c1_2->SetFrameBorderMode(0);
   
   TH1D *hsig__62 = new TH1D("hsig__62","mus_sigid",2,0,2);
   hsig__62->SetBinContent(1,109);
   hsig__62->SetBinContent(2,621);
   hsig__62->SetMinimum(0);
   hsig__62->SetEntries(730);
   hsig__62->SetStats(0);

   ci = TColor::GetColor("#000099");
   hsig__62->SetLineColor(ci);
   hsig__62->GetXaxis()->SetRange(1,99);
   hsig__62->GetXaxis()->SetLabelFont(42);
   hsig__62->GetXaxis()->SetLabelSize(0.035);
   hsig__62->GetXaxis()->SetTitleSize(0.035);
   hsig__62->GetXaxis()->SetTitleFont(42);
   hsig__62->GetYaxis()->SetLabelFont(42);
   hsig__62->GetYaxis()->SetLabelSize(0.035);
   hsig__62->GetYaxis()->SetTitleSize(0.035);
   hsig__62->GetYaxis()->SetTitleOffset(0);
   hsig__62->GetYaxis()->SetTitleFont(42);
   hsig__62->GetZaxis()->SetLabelFont(42);
   hsig__62->GetZaxis()->SetLabelSize(0.035);
   hsig__62->GetZaxis()->SetTitleSize(0.035);
   hsig__62->GetZaxis()->SetTitleFont(42);
   hsig__62->Draw("");
   
   pt = new TPaveText(0.3628994,0.9348829,0.6371006,0.995,"blNDC");
   pt->SetName("title");
   pt->SetBorderSize(0);
   pt->SetFillColor(0);
   pt->SetFillStyle(0);
   pt->SetTextFont(42);
   pt_LaTex = pt->AddText("mus_sigid");
   pt->Draw();
   c1_2->Modified();
   c1->cd();
  
// ------------>Primitives in pad: c1_3
   TPad *c1_3 = new TPad("c1_3", "c1_3",0.6766667,0.01,0.99,0.99);
   c1_3->Draw();
   c1_3->cd();
   c1_3->Range(-0.25,-177.975,2.25,1601.775);
   c1_3->SetFillColor(0);
   c1_3->SetBorderMode(0);
   c1_3->SetBorderSize(2);
   c1_3->SetFrameBorderMode(0);
   c1_3->SetFrameBorderMode(0);
   
   TH1D *hdat__63 = new TH1D("hdat__63","mus_sigid",2,0,2);
   hdat__63->SetBinContent(1,774);
   hdat__63->SetBinContent(2,1356);
   hdat__63->SetMinimum(0);
   hdat__63->SetEntries(2130);
   hdat__63->SetStats(0);

   ci = TColor::GetColor("#000099");
   hdat__63->SetLineColor(ci);
   hdat__63->GetXaxis()->SetRange(1,99);
   hdat__63->GetXaxis()->SetLabelFont(42);
   hdat__63->GetXaxis()->SetLabelSize(0.035);
   hdat__63->GetXaxis()->SetTitleSize(0.035);
   hdat__63->GetXaxis()->SetTitleFont(42);
   hdat__63->GetYaxis()->SetLabelFont(42);
   hdat__63->GetYaxis()->SetLabelSize(0.035);
   hdat__63->GetYaxis()->SetTitleSize(0.035);
   hdat__63->GetYaxis()->SetTitleOffset(0);
   hdat__63->GetYaxis()->SetTitleFont(42);
   hdat__63->GetZaxis()->SetLabelFont(42);
   hdat__63->GetZaxis()->SetLabelSize(0.035);
   hdat__63->GetZaxis()->SetTitleSize(0.035);
   hdat__63->GetZaxis()->SetTitleFont(42);
   hdat__63->Draw("");
   
   pt = new TPaveText(0.3628994,0.9348829,0.6371006,0.995,"blNDC");
   pt->SetName("title");
   pt->SetBorderSize(0);
   pt->SetFillColor(0);
   pt->SetFillStyle(0);
   pt->SetTextFont(42);
   pt_LaTex = pt->AddText("mus_sigid");
   pt->Draw();
   c1_3->Modified();
   c1->cd();
   c1->Modified();
   c1->cd();
   c1->SetSelected(c1);
}
