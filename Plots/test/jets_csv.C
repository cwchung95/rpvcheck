void jets_csv()
{
//=========Macro generated from canvas: c1/c1
//=========  (Thu Dec 26 14:41:01 2019) by ROOT version 6.12/07
   TCanvas *c1 = new TCanvas("c1", "c1",0,23,2400,800);
   c1->SetHighLightColor(2);
   c1->Range(0,0,1,1);
   c1->SetFillColor(0);
   c1->SetBorderMode(0);
   c1->SetBorderSize(2);
   c1->SetFrameBorderMode(0);
  
// ------------>Primitives in pad: c1_1
   TPad *c1_1 = new TPad("c1_1", "c1_1",0.01,0.01,0.3233333,0.99);
   c1_1->Draw();
   c1_1->cd();
   c1_1->Range(-12.625,-453.4688,3.625,4081.219);
   c1_1->SetFillColor(0);
   c1_1->SetBorderMode(0);
   c1_1->SetBorderSize(2);
   c1_1->SetFrameBorderMode(0);
   c1_1->SetFrameBorderMode(0);
   
   TH1D *hbkg__109 = new TH1D("hbkg__109","jets_csv",99,-11,2);
   hbkg__109->SetBinContent(8,1809);
   hbkg__109->SetBinContent(85,3455);
   hbkg__109->SetBinContent(86,2935);
   hbkg__109->SetBinContent(87,1548);
   hbkg__109->SetBinContent(88,1477);
   hbkg__109->SetBinContent(89,768);
   hbkg__109->SetBinContent(90,791);
   hbkg__109->SetBinContent(91,1114);
   hbkg__109->SetBinContent(92,1037);
   hbkg__109->SetMinimum(0);
   hbkg__109->SetEntries(14934);
   hbkg__109->SetStats(0);

   Int_t ci;      // for color index setting
   TColor *color; // for color definition with alpha
   ci = TColor::GetColor("#000099");
   hbkg__109->SetLineColor(ci);
   hbkg__109->GetXaxis()->SetRange(1,99);
   hbkg__109->GetXaxis()->SetLabelFont(42);
   hbkg__109->GetXaxis()->SetLabelSize(0.035);
   hbkg__109->GetXaxis()->SetTitleSize(0.035);
   hbkg__109->GetXaxis()->SetTitleFont(42);
   hbkg__109->GetYaxis()->SetLabelFont(42);
   hbkg__109->GetYaxis()->SetLabelSize(0.035);
   hbkg__109->GetYaxis()->SetTitleSize(0.035);
   hbkg__109->GetYaxis()->SetTitleOffset(0);
   hbkg__109->GetYaxis()->SetTitleFont(42);
   hbkg__109->GetZaxis()->SetLabelFont(42);
   hbkg__109->GetZaxis()->SetLabelSize(0.035);
   hbkg__109->GetZaxis()->SetTitleSize(0.035);
   hbkg__109->GetZaxis()->SetTitleFont(42);
   hbkg__109->Draw("");
   
   TPaveText *pt = new TPaveText(0.387521,0.9348829,0.612479,0.995,"blNDC");
   pt->SetName("title");
   pt->SetBorderSize(0);
   pt->SetFillColor(0);
   pt->SetFillStyle(0);
   pt->SetTextFont(42);
   TText *pt_LaTex = pt->AddText("jets_csv");
   pt->Draw();
   c1_1->Modified();
   c1->cd();
  
// ------------>Primitives in pad: c1_2
   TPad *c1_2 = new TPad("c1_2", "c1_2",0.3433333,0.01,0.6566667,0.99);
   c1_2->Draw();
   c1_2->cd();
   c1_2->Range(-12.625,-204.3563,3.625,1839.206);
   c1_2->SetFillColor(0);
   c1_2->SetBorderMode(0);
   c1_2->SetBorderSize(2);
   c1_2->SetFrameBorderMode(0);
   c1_2->SetFrameBorderMode(0);
   
   TH1D *hsig__110 = new TH1D("hsig__110","jets_csv",99,-11,2);
   hsig__110->SetBinContent(8,775);
   hsig__110->SetBinContent(85,1557);
   hsig__110->SetBinContent(86,1472);
   hsig__110->SetBinContent(87,709);
   hsig__110->SetBinContent(88,695);
   hsig__110->SetBinContent(89,456);
   hsig__110->SetBinContent(90,476);
   hsig__110->SetBinContent(91,699);
   hsig__110->SetBinContent(92,683);
   hsig__110->SetMinimum(0);
   hsig__110->SetEntries(7522);
   hsig__110->SetStats(0);

   ci = TColor::GetColor("#000099");
   hsig__110->SetLineColor(ci);
   hsig__110->GetXaxis()->SetRange(1,99);
   hsig__110->GetXaxis()->SetLabelFont(42);
   hsig__110->GetXaxis()->SetLabelSize(0.035);
   hsig__110->GetXaxis()->SetTitleSize(0.035);
   hsig__110->GetXaxis()->SetTitleFont(42);
   hsig__110->GetYaxis()->SetLabelFont(42);
   hsig__110->GetYaxis()->SetLabelSize(0.035);
   hsig__110->GetYaxis()->SetTitleSize(0.035);
   hsig__110->GetYaxis()->SetTitleOffset(0);
   hsig__110->GetYaxis()->SetTitleFont(42);
   hsig__110->GetZaxis()->SetLabelFont(42);
   hsig__110->GetZaxis()->SetLabelSize(0.035);
   hsig__110->GetZaxis()->SetTitleSize(0.035);
   hsig__110->GetZaxis()->SetTitleFont(42);
   hsig__110->Draw("");
   
   pt = new TPaveText(0.387521,0.9348829,0.612479,0.995,"blNDC");
   pt->SetName("title");
   pt->SetBorderSize(0);
   pt->SetFillColor(0);
   pt->SetFillStyle(0);
   pt->SetTextFont(42);
   pt_LaTex = pt->AddText("jets_csv");
   pt->Draw();
   c1_2->Modified();
   c1->cd();
  
// ------------>Primitives in pad: c1_3
   TPad *c1_3 = new TPad("c1_3", "c1_3",0.6766667,0.01,0.99,0.99);
   c1_3->Draw();
   c1_3->cd();
   c1_3->Range(-12.625,-1847.213,3.625,16624.91);
   c1_3->SetFillColor(0);
   c1_3->SetBorderMode(0);
   c1_3->SetBorderSize(2);
   c1_3->SetFrameBorderMode(0);
   c1_3->SetFrameBorderMode(0);
   
   TH1D *hdat__111 = new TH1D("hdat__111","jets_csv",99,-11,2);
   hdat__111->SetBinContent(8,7128);
   hdat__111->SetBinContent(84,1);
   hdat__111->SetBinContent(85,14074);
   hdat__111->SetBinContent(86,12425);
   hdat__111->SetBinContent(87,6110);
   hdat__111->SetBinContent(88,5890);
   hdat__111->SetBinContent(89,2672);
   hdat__111->SetBinContent(90,2107);
   hdat__111->SetBinContent(91,1713);
   hdat__111->SetBinContent(92,943);
   hdat__111->SetMinimum(0);
   hdat__111->SetEntries(53063);
   hdat__111->SetStats(0);

   ci = TColor::GetColor("#000099");
   hdat__111->SetLineColor(ci);
   hdat__111->GetXaxis()->SetRange(1,99);
   hdat__111->GetXaxis()->SetLabelFont(42);
   hdat__111->GetXaxis()->SetLabelSize(0.035);
   hdat__111->GetXaxis()->SetTitleSize(0.035);
   hdat__111->GetXaxis()->SetTitleFont(42);
   hdat__111->GetYaxis()->SetLabelFont(42);
   hdat__111->GetYaxis()->SetLabelSize(0.035);
   hdat__111->GetYaxis()->SetTitleSize(0.035);
   hdat__111->GetYaxis()->SetTitleOffset(0);
   hdat__111->GetYaxis()->SetTitleFont(42);
   hdat__111->GetZaxis()->SetLabelFont(42);
   hdat__111->GetZaxis()->SetLabelSize(0.035);
   hdat__111->GetZaxis()->SetTitleSize(0.035);
   hdat__111->GetZaxis()->SetTitleFont(42);
   hdat__111->Draw("");
   
   pt = new TPaveText(0.387521,0.9348829,0.612479,0.995,"blNDC");
   pt->SetName("title");
   pt->SetBorderSize(0);
   pt->SetFillColor(0);
   pt->SetFillStyle(0);
   pt->SetTextFont(42);
   pt_LaTex = pt->AddText("jets_csv");
   pt->Draw();
   c1_3->Modified();
   c1->cd();
   c1->Modified();
   c1->cd();
   c1->SetSelected(c1);
}
