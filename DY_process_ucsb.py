import os
import sys
import ROOT
import numpy as np
from ROOT import THStack, TChain, TH1D, TColor, TFile, TLorentzVector

datdirec = "/xrootd_user/jaehyeok/xrootd/ucsb_babies/2017_02_14/data/merged_rpvdata_rpvfit/"
mcdirec = "/xrootd_user/jaehyeok/xrootd/ucsb_babies/2017_01_27/mc/merged_rpvmc_rpvfit/"
datfile = TChain("tree")
datfile.Add(datdirec+"*JetHT*")

qcdfile = TChain("tree")
qcdfile.Add(mcdirec+"*_QCD_*")

wjetsfile = TChain("tree")
wjetsfile.Add(mcdirec+"*_WJetsToLNu_*")

ttfile = TChain("tree")
ttfile.Add(mcdirec+"*_TTJets_*")

zjetsfile = TChain("tree")
zjetsfile.Add(mcdirec+"*_DYJetsToLL_M-50*")
zjetsfile.Add(mcdirec+"*_ZJetsToQQ_HT600toInf_*")
zjetsfile.Add(mcdirec+"*_ZJetsToNuNu_HT-*")

othersfile = TChain("tree")
othersfile.Add(mcdirec+"*_WZ_*")
othersfile.Add(mcdirec+"*_ZZ_*")
othersfile.Add(mcdirec+"*_WW_*")
othersfile.Add(mcdirec+"*_WZZ_*")
othersfile.Add(mcdirec+"*_WWZ_*")
othersfile.Add(mcdirec+"*_ZZZ_*")
othersfile.Add(mcdirec+"*_WWW_*")
othersfile.Add(mcdirec+"*_ST_*")
othersfile.Add(mcdirec+"*_TTTT_*")
othersfile.Add(mcdirec+"*_TTZToLLNuNu_*")
othersfile.Add(mcdirec+"*_TTZToQQ_*")
othersfile.Add(mcdirec+"*_TTWJetsToLNu_*")

nbcuts = ["nbm==0","nbm==1","nbm==2","nbm==3","nbm>=4"]
njetscuts = ["njets>=4 && njets<=5","njets>=6 && njets<=7","njets>=8"]

he = TH1D("he","he",20,0,200)
hm = TH1D("hm","hm",20,0,200)

#c = ROOT.TCanvas("c1","c1",2400,800)

#c.Divide(3,1)

theta = ["2*atan(exp(-leps_eta[0]))","2*atan(exp(-leps_eta[1]))"]
phi = ["leps_phi[0]","leps_phi[1]"]
pt = ["leps_pt[0]","leps_pt[1]"]

px = list(pt+"*cos("+phi+")" for pt,phi in zip(pt,phi))
py = list(pt+"*sin("+phi+")" for pt,phi in zip(pt,phi))
pz = list(pt+"/(tan("+theta+"))" for pt,theta in zip(pt,theta))

pnorm = list("sqrt("+pt+"^2+"+pz+"^2)" for pt,pz in zip(pt,pz))
mll = "sqrt(2*("+pnorm[0]+"*"+pnorm[1]+"-("+px[0]+"*"+px[1]+"+"+py[0]+"*"+py[1]+"+"+pz[0]+"*"+pz[1]+")))"

mumu_sel = "leps_id[0]*leps_id[1] == -169"
elel_sel = "leps_id[0]*leps_id[1] == -121"

datfile.Draw(mll+">>hm","nleps==2 &&"+mumu_sel,"goff")
datfile.Draw(mll+">>he","nleps==2 &&"+elel_sel,"goff")

MCFiles = [qcdfile, ttfile, wjetsfile, zjetsfile, othersfile]
color = [400-7,860+7,416+2,632-7,920+1]

"""
hmcmu = TH1D("hmcmu","hmcmu",20,0,200)
hmcel = TH1D("hmcel","hmcel",20,0,200)
hmcstmu = THStack("hmcstmu","")
hmcstel = THStack("hmcstel","")
hmcstot = THStack("hmcsttot","")
hmcstackmll.SetStats(0)
histmc = []
histmc = []
for process in MCFiles :
	process.Draw(mll+">>hmcmu","weight*35.9*(nleps==2 &&"+mumu_sel+")","goff")
	process.Draw(mll+">>hmcel","weight*35.9*(nleps==2 &&"+elel_sel+")","goff")
	hmcmu.SetFillColor(color[MCFiles.index(process)])
	hmcel.SetFillColor(color[MCFiles.index(process)])
	hmu = hmcmu.Clone()
	hel = hmcel.Clone()
	hmcstmu.Add(hmu)
	hmcstel.Add(hel)
	htot = hmcmu.Clone()
	htot.Add(hmcel)
	hmcstot.Add(htot)

ymaxml = max(hmcstot.GetMaximum()*1.3,(he.GetMaximum()+hm.GetMaximum())*1.3)

c.Divide(3)
c.cd(1)
he.SetTitle("Z/gamma to el- & el+")
he.SetStats(0)
hmcstel.SetMaximum(ymaxml)
hmcstel.Draw("hist")
he.Draw("same e")

c.cd(2)
hm.SetTitle("Z/gamma to mu- & mu+")
hm.SetStats(0)
hmcstmu.SetMaximum(ymaxml)
hmcstmu.Draw("hist")
hm.Draw("same e")

c.cd(3)
hst = hm.Clone()
hst.Add(he)
hst.SetTitle("Z to lep- & lep+")
hst.SetMaximum(ymaxml)
hmcstot.Draw("hist")
hst.Draw("same e")
c.Print("./Plots/Ztollbar.pdf")
#c.Print("Plots/DY_wjets_Comp/DY_process_"+nb+"_"+njets+".pdf")

hq = TH1D("hq","hq",3,4,9)
hw = TH1D("hw","hw",3,4,9)
htt = TH1D("htt","htt",3,4,9)
hz = TH1D("hz","hz",3,4,9)
ho = TH1D("ho","ho",3,4,9)

hnj = ["hq", "htt", "hw", "hz", "ho"]

hd = TH1D("hd","hd",3,4,9)

datfile.Draw("min(njets,9-0.0000001)>>hd","ht>1200 && mj12>500 && nleps==2 &&"+mll+">=80 && "+mll+"<=100 && ("+mumu_sel+"||"+elel_sel+") && (trig_ht900 || trig_jet450)","goff")

for proc in MCFiles:
	proc.Draw("min(njets,9-0.0000001)>>"+hnj[MCFiles.index(proc)],"weight*35.9*(ht>1200 && mj12>500 && nleps==2 &&"+mll+">=80 && "+mll+"<=100 && ("+mumu_sel+"||"+elel_sel+"))","goff")

hstnj = THStack("hstnj","")
c1 = ROOT.TCanvas("c1","c1",800,800)
c1.cd()
hd.SetStats(0)
hq.SetFillColor(400-7)
htt.SetFillColor(860+7)
hw.SetFillColor(416+2)
hz.SetFillColor(632-2)
ho.SetFillColor(920+1)

hstnj.Add(ho)
hstnj.Add(hw)
hstnj.Add(htt)
hstnj.Add(hq)
hstnj.Add(hz)

ymax = max(hstnj.GetMaximum(),hd.GetMaximum())

hstnj.SetMaximum(ymax*1.3)
hstnj.Draw("hist")
hd.Draw("same e")

c1.Print("./Plots/njets_distribution_DY.pdf")
c1.Print("./Plots/njets_distribution_DY.C")
"""

#TLorentzVector's mll method#
# Making Event Loops #

yld = [ datfile.GetEntries(), qcdfile.GetEntries(), ttfile.GetEntries(), zjetsfile.GetEntries(), wjetsfile.GetEntries(), othersfile.GetEntries() ]
ALLFiles = [ datfile, qcdfile, ttfile, wjetsfile, zjetsfile, othersfile ]
histproc = []
hmcstackmll = THStack("mll","")
hmcstacknj = THStack("njets","")
hstnjb0 = THStack("njets","")
hstnjb1 = THStack("njets","")
for proc in ALLFiles:
	hmll2 = TH1D("hmll2","hmll2",20,0,200)
	hnjets = TH1D("hnjets","hnjets",3,4,10)
	hnjb0 = TH1D("hnjb0","hnjb0",3,4,10)
	hnjb1 = TH1D("hnjb1","hnjb1",3,4,10)
	for etry in range(0, yld[ALLFiles.index(proc)]-1):
		proc.GetEntry(etry)
		#if proc.pass != 1 : continue
		if proc.nleps != 2 : continue
		mass = 0		
		if proc.leps_id.at(0)*proc.leps_id.at(1) == -121 : mass = 0#0.510*pow(10,-3)
		elif proc.leps_id.at(0)*proc.leps_id.at(1) == -169 : mass = 0#105.66*pow(10,-3) 
		if not proc.leps_id.at(0)*proc.leps_id.at(1) == -121 and not proc.leps_id.at(0)*proc.leps_id.at(1) == -169 : continue#105.66*pow(10,-3) 
		if proc.leps_pt.at(0) < 30 : continue
		mom1 = TLorentzVector(0,0,0,0)
		mom2 = TLorentzVector(0,0,0,0)
		mom1.SetPtEtaPhiM(proc.leps_pt.at(0), proc.leps_eta.at(0), proc.leps_phi.at(0), mass)	
		mom2.SetPtEtaPhiM(proc.leps_pt.at(1), proc.leps_eta.at(1), proc.leps_phi.at(1), mass)
		momtot = mom1+mom2
		mll2 = momtot.M()
		if proc == datfile : wgt = 1
		else : wgt = proc.weight*35.9
		hmll2.Fill(mll2,wgt)
		if mll2 < 80 or mll2 > 100 : continue
		if proc.ht < 1200 : continue
		if proc.mj12 < 500 : continue
		if proc == datfile:
			if not proc.trig.at(12) and not proc.trig.at(54) and not proc.trig.at(56) : continue
		else :
			if not proc.stitch_ht : continue
		hnjets.Fill(min(proc.njets,10-0.000001),wgt)
		if proc.nbm == 0: hnjb0.Fill(min(proc.njets,10-0.000001),wgt)
		if proc.nbm >= 1: hnjb1.Fill(min(proc.njets,10-0.000001),wgt)
	hist_mll = hmll2.Clone()
	hist_njets = hnjets.Clone()
	hist_njb0 = hnjb0.Clone()
	hist_njb1 = hnjb1.Clone()
	hmll2.Reset()
	hnjets.Reset()
	hnjb0.Reset()
	hnjb1.Reset()
	if proc == datfile : 
		hist_dat_mll = hist_mll
		hist_dat_njets = hist_njets
		hist_dat_njb0 = hist_njb0
		hist_dat_njb1 = hist_njb1
	else :	
		hist_mll.SetFillColor(color[ALLFiles.index(proc)-1]) 
		hist_njets.SetFillColor(color[ALLFiles.index(proc)-1])
		hmcstackmll.Add(hist_mll)
		hmcstacknj.Add(hist_njets)
		hist_njb0.SetFillColor(color[ALLFiles.index(proc)-1])
		hist_njb1.SetFillColor(color[ALLFiles.index(proc)-1])	
		hstnjb0.Add(hist_njb0)
		hstnjb1.Add(hist_njb1)

c2 = ROOT.TCanvas("c2","c2",1600,800)
c2.Divide(2,1)
c2.cd(1)
ymaxi1 = max(hist_dat_mll.GetMaximum(),hmcstackmll.GetMaximum())
ymaxi2 = max(hist_dat_njets.GetMaximum(),hmcstacknj.GetMaximum())
hmcstackmll.SetMaximum(ymaxi1*1.3)
hmcstackmll.Draw("hist")
hist_dat_mll.Draw("same e")
c2.cd(2)
hmcstacknj.SetMaximum(ymaxi2*1.3)
hmcstacknj.Draw("hist")
hist_dat_njets.Draw("same e")
c2.Print("./Plots/TLorentzVector_ucsb.pdf")
c2.Print("./Plots/TLorentzVector_uvsb.C")

c3 = ROOT.TCanvas("c3","c3",1600,800)
c3.Divide(2,1)
c3.cd(1)
ymaxib0 = max(hist_dat_njb0.GetMaximum(),hstnjb0.GetMaximum())
ymaxib1 = max(hist_dat_njb1.GetMaximum(),hstnjb1.GetMaximum())
hstnjb0.SetMaximum(ymaxib0*1.3)
hstnjb0.Draw("hist")
hist_dat_njb0.Draw("same e")
c3.cd(2)
hstnjb1.SetMaximum(ymaxib1*1.3)
hstnjb1.Draw("hist")
hist_dat_njb1.Draw("same e")
c3.Print("./Plots/TLorentzVectorMethodwithnb0andnb1_ucsb.pdf")
c3.Print("./Plots/TLorentzVectorMethodwithnb0andnb1_ucsb.C")
